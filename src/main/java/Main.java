import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;


public class Main {

    public static void main (String args []) throws IOException, ParseException {
            File file = new File("data.csv");
            String csvSplitter = ";";
            String line;
            List<String> Dates = new ArrayList<>();
            Map<Date, Date> DateList = new HashMap<>();
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
            Dates.add(line);
            }
            Dates.remove(0);

            for (String s : Dates) {
                SimpleDateFormat a = new SimpleDateFormat("yyyyMMdd");
               String[] cols = s.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                Date DateCorrect1 = null;
                Date DateCorrect2 = null;
                try {
                    DateCorrect1 = a.parse(cols[10]);
                    DateCorrect2 = a.parse(cols[11]);
                    DateList.put(DateCorrect1, DateCorrect2);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
//                System.out.println(cols[10] + csvSplitter + cols[11]);
              }
            Date a = dateInput();
            compare(a,DateList);
        }

    public static Date dateInput() throws ParseException {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a date(yyyy/MM/dd) from 2012 to 2020: ");
        String userDate = in.nextLine();
        SimpleDateFormat s = new SimpleDateFormat("yyyy/MM/dd");
        Date userDateCorrect = null;
        userDateCorrect = s.parse(userDate);
        System.out.println("Your date: " + userDate);
        return userDateCorrect;
    }
    public static void compare(Date userDateCorrect, Map Datelist) {

       AtomicReference<Integer> counter = new AtomicReference<>(0);
        Integer index = 0;
        Datelist.forEach((k,v) -> {
            if (userDateCorrect.after((Date) k) & userDateCorrect.before((Date) v) || userDateCorrect == k || userDateCorrect == v)
            counter.updateAndGet(v1 -> v1 + 1);});
        System.out.println("Restrictions count: " + counter);
                }
}

